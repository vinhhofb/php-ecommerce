<nav class="sidebar sidebar-offcanvas" id="sidebar" style="position:fixed;">
  <ul class="nav">
    <li class="nav-item">
      <a class="nav-link" href="{{url('kenh-cua-hang/quan-ly-cua-hang')}}">
        <i class="icon-grid-2 menu-icon"></i>
        <span class="menu-title">Danh sách cửa hàng</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" data-toggle="collapse" href="#tables" aria-expanded="false" aria-controls="tables">
        <i class="icon-contract menu-icon"></i>
        <span class="menu-title">Thiết lập</span>
        <i class="menu-arrow"></i>
      </a>
      <div class="collapse" id="tables">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item"> <a class="nav-link" href="{{url('kenh-cua-hang/thong-tin-tai-khoan')}}">Thông tin tài khoản</a></li>
          <li class="nav-item"> <a class="nav-link" href="{{url('kenh-cua-hang/doi-mat-khau')}}">Đổi mật khẩu</a></li>
        </ul>
      </div>
    </li>

  </ul>
</nav>

